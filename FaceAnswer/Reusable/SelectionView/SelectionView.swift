//
//  SelectionView.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 9.08.2022.
//

import UIKit

protocol SelectionViewDelegate: AnyObject {
    func selectionViewDidTap(_ selectionView: SelectionView)
}

final class SelectionView: UIView {
    
    weak var delegate: SelectionViewDelegate?
    
    private var titleLabel: UILabel!
    
    var text: String? {
        get { titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    var textColor: UIColor {
        get { titleLabel.textColor }
        set { titleLabel.textColor = newValue }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        loadViews()
        loadConstraints()
    }
}


// MARK: - View

private extension SelectionView {
    
    func loadViews() {
        configureSelf()
        loadTitleView()
    }
    
    func configureSelf() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectionViewDidTap))
        addGestureRecognizer(gestureRecognizer)
        backgroundColor = .orange
        layer.cornerRadius = 4
    }
    
    func loadTitleView() {
        let titleView = UILabel()
        titleView.font = .systemFont(ofSize: 24, weight: .bold)
        titleView.textAlignment = .center
        titleView.textColor = .white
        titleView.numberOfLines = 0
        self.titleLabel = titleView
        addSubview(titleView)
    }
}

// MARK: - Constraints

private extension SelectionView {
    
    func loadConstraints() {
        loadTitleViewConstraints()
    }
    
    func loadTitleViewConstraints() {
        titleLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(12)
        }
    }
}

// MARK: - Target actions

@objc private extension SelectionView {
    
    func selectionViewDidTap() {
        delegate?.selectionViewDidTap(self)
    }
}
