//
//  UIViewController+Extensions.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 10.08.2022.
//

import UIKit

extension UIViewController {
    
    func presentErrorAlert(with description: String) {
        let alertController = UIAlertController(title: "Error", message: description, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default)
        alertController.addAction(okAction)
        present(alertController, animated: true)
    }
}
