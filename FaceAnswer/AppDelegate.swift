//
//  AppDelegate.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 8.08.2022.
//

import UIKit
import CoreData
import SnapKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window:UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        // initiliaze home screen
        let homeViewController = HomeViewController()
        let navigationController = UINavigationController(rootViewController: homeViewController)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }
    
    // MARK: - CoreData
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "FaceAnswer")
        container.loadPersistentStores { _, error in
            if let error = error {
                print(error.localizedDescription)
                print(error)
                fatalError()
            }
        }
        return container
    }()
    
    func saveContext() throws {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            try context.save()
        }
    }
}

