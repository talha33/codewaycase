//
//  User.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 10.08.2022.
//

import Foundation

final class User {
    
    var username: String? {
        get {
            if let username = UserDefaults.standard.string(forKey: "username") {
                return username
            } else {
                return nil   
            }
        }
        set { UserDefaults.standard.set(newValue, forKey: "username") }
    }
    
    static let shared = User()
    
    private init() { }
}
