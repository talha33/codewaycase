//
//  CoreDataManager.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 9.08.2022.
//

import CoreData
import UIKit

final class CoreDataManager {
    
    static func saveScoreData(with data: Score) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let viewContext = appDelegate.persistentContainer.viewContext
        if let entity = NSEntityDescription.entity(forEntityName: Constant.entityName, in: viewContext) {
            let scoreObject = NSManagedObject(entity: entity, insertInto: viewContext)
            scoreObject.setValue(data.score, forKey: Constant.scoreKeyPath)
            scoreObject.setValue(data.username, forKey: Constant.usernameKeyPath)
            scoreObject.setValue(data.date, forKey: Constant.dateKeyPath)
        }
        try? viewContext.save()
    }
    
    static func getScoreData() -> [Score] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [] }
        
        let viewContext = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Constant.entityName)
        let results = try? viewContext.fetch(request)
        var scores: [Score] = []
        if let dataObjects = results as? [NSManagedObject] {
            for dataObject in dataObjects {
                guard let scoreValue = dataObject.value(forKey: Constant.scoreKeyPath) as? Int,
                      let username = dataObject.value(forKey: Constant.usernameKeyPath) as? String,
                      let date = dataObject.value(forKey: Constant.dateKeyPath) as? Date else { continue }
                let score = Score(score: scoreValue, username: username, date: date)
                scores.append(score)
            }
        }
        
        return scores
    }
    
    private enum Constant {
        static let entityName = "Scores"
        static let scoreKeyPath = "score"
        static let usernameKeyPath = "username"
        static let dateKeyPath = "date"
    }
}
