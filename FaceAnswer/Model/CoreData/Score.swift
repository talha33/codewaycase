//
//  Score.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 11.08.2022.
//

import Foundation

struct Score: Equatable {
    
    let score: Int
    let username: String
    let date: Date
}
