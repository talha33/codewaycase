//
//  GameManager.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 8.08.2022.
//

import Foundation

protocol GameManagerDelegate: AnyObject {
    
    func gameManagerAnswerTimeWillBegin(with question: Question)
    func gameManagerAnswerTimeDidEndWithoutResponse(result: GameManager.AnswerResult)
    func gameManagerWaitTimeWillBegin()
    func gameManagerWaitTimeDidEnd()
    func gameManagerGameDidEnd(with score: Int)
    func gameManagerTimerProgress(progress: Double)
}

final class GameManager {
    
    weak var delegate: GameManagerDelegate?
    
    private var timer: Timer?
    private var progressTimer: Timer?
    private var questions: [Question]
    private var score = 0
    private var currentIndex = 0
    
    var currentQuestion: Question {
        questions[currentIndex]
    }
    
    init(content: QuestionManager.ContentType) {
        questions = QuestionManager().questions(for: content)
    }
}

// MARK: - Helpers

extension GameManager {
    
    enum AnswerResult {
        case correct(correctIndex: Int)
        case wrong(selectedIndex: Int, correctIndex: Int)
        case missed(correctIndex: Int)
    }
    
    func startGame() {
        scheduleAnswerTimer()
    }
    
    func checkAnswer(with index: Int) -> AnswerResult {
        scheduleWaitTimer()
        if index == currentQuestion.rightAnswerIndex {
            score += 1
            return .correct(correctIndex: index)
        } else {
            return .wrong(selectedIndex: index, correctIndex: currentQuestion.rightAnswerIndex)
        }
    }
}

// MARK: - Timers

private extension GameManager {
    
    func scheduleAnswerTimer() {
        timer?.invalidate()
        progressTimer?.invalidate()
        delegate?.gameManagerAnswerTimeWillBegin(with: currentQuestion)
        timer = Timer.scheduledTimer(timeInterval: Constants.Timer.answerTime,
                                     target: self,
                                     selector: #selector(answerTimeDidEnd),
                                     userInfo: nil,
                                     repeats: false)
        progressTimer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true, block: { timer in
            let timeDiff = self.timer?.fireDate.timeIntervalSince(Date()) ?? 0
            self.delegate?.gameManagerTimerProgress(progress: timeDiff / Constants.Timer.answerTime)
        })
    }
    
    func scheduleWaitTimer() {
        timer?.invalidate()
        progressTimer?.invalidate()
        delegate?.gameManagerWaitTimeWillBegin()
        timer = Timer.scheduledTimer(timeInterval: Constants.Timer.nextQuestionGap,
                                     target: self,
                                     selector: #selector(waitTimeDidEnd),
                                     userInfo: nil,
                                     repeats: false)
        progressTimer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true, block: { timer in
            let timeDiff = self.timer?.fireDate.timeIntervalSince(Date()) ?? 0
            self.delegate?.gameManagerTimerProgress(progress: timeDiff / Constants.Timer.nextQuestionGap)
        })
        
    }
}

// MARK: - Target Actions

@objc private extension GameManager {
    
    func answerTimeDidEnd() {
        progressTimer?.invalidate()
        timer?.invalidate()
        delegate?.gameManagerAnswerTimeDidEndWithoutResponse(result: .missed(correctIndex: currentQuestion.rightAnswerIndex))
        scheduleWaitTimer()
    }
    
    func waitTimeDidEnd() {
        progressTimer?.invalidate()
        timer?.invalidate()
        currentIndex += 1
        if currentIndex >= questions.count {
            delegate?.gameManagerGameDidEnd(with: score)
            return
        }
        delegate?.gameManagerWaitTimeDidEnd()
        scheduleAnswerTimer()
    }
}

// MARK: - Constants

private extension GameManager {
    
    enum Constants {
        
        enum Timer {
            
            static let answerTime: TimeInterval = 10
            static let nextQuestionGap: TimeInterval = 3
        }
    }
}
