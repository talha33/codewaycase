//
//  FootballQuestion.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 8.08.2022.
//

import Foundation

struct SwiftQuestion: Question {
    
    static let topic = "Swift"
    var question: String
    var answers: [String]
    var rightAnswerIndex: Int
}

