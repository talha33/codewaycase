//
//  QuestionManager.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 8.08.2022.
//

import Foundation

final class MathQuestionStorage {
    
    static let questions: [Question] = [
        MathQuestion(question: "3 x 9", answers: ["27", "9"], rightAnswerIndex: 0),
        MathQuestion(question: "1 x 9", answers: ["27", "9"], rightAnswerIndex: 1),
        MathQuestion(question: "3 x 19", answers: ["27", "57"], rightAnswerIndex: 1),
        MathQuestion(question: "49 x 9", answers: ["490", "441"], rightAnswerIndex: 1),
        MathQuestion(question: "21 x 10", answers: ["220", "210"], rightAnswerIndex: 1),
        MathQuestion(question: "30 x 21", answers: ["630", "660"], rightAnswerIndex: 0),
        MathQuestion(question: "12 x 12", answers: ["144", "122"], rightAnswerIndex: 0),
        MathQuestion(question: "12 x 20", answers: ["240", "120"], rightAnswerIndex: 0),
        MathQuestion(question: "11 x 0", answers: ["0", "1"], rightAnswerIndex: 0),
        MathQuestion(question: "11 x 1", answers: ["1", "11"], rightAnswerIndex: 1),
        MathQuestion(question: "9 / 3 + 1", answers: ["4", "2.25"], rightAnswerIndex: 0),
        MathQuestion(question: "1 x 5", answers: ["1", "5"], rightAnswerIndex: 1),
        MathQuestion(question: "3 x 10", answers: ["20", "30"], rightAnswerIndex: 1),
        MathQuestion(question: "49 x 2", answers: ["99", "98"], rightAnswerIndex: 1),
        MathQuestion(question: "21 x 5", answers: ["110", "105"], rightAnswerIndex: 1),
        MathQuestion(question: "40 / 2", answers: ["20", "10"], rightAnswerIndex: 0),
        MathQuestion(question: "10 ^ 1", answers: ["10", "100"], rightAnswerIndex: 0),
        MathQuestion(question: "10 ^ 3", answers: ["1000", "100"], rightAnswerIndex: 0),
        MathQuestion(question: "10 ^ 2", answers: ["100", "10"], rightAnswerIndex: 0),
        MathQuestion(question: "10 ^ -1", answers: ["1", "0.1"], rightAnswerIndex: 1)
    ]
}

final class SwiftQuestionStorage {
    
    static let questions: [Question] = [
        SwiftQuestion(question: "Which one is proper at Swift?", answers: ["isValid ? foo()", "var x: Int { mutating get { foo() } }"], rightAnswerIndex: 1),
        SwiftQuestion(question: "What is the use of double question marks?", answers: ["Default value", "Optional"], rightAnswerIndex: 0),
        SwiftQuestion(question: "What is the use of exclamation mark", answers: ["force unwrap", "optional"], rightAnswerIndex: 0),
        SwiftQuestion(question: "What is let keyword in swift", answers: ["constant", "variable"], rightAnswerIndex: 0),
        SwiftQuestion(question: "Which code block execute after return", answers: ["deinit", "defer"], rightAnswerIndex: 1),
        SwiftQuestion(question: "var x = true ? 3 : 2", answers: ["3", "2"], rightAnswerIndex: 0),
        SwiftQuestion(question: "var y = 0.3 | What is the type of y?", answers: ["Float", "Double"], rightAnswerIndex: 1),
        SwiftQuestion(question: "Which map provide non optional array?", answers: ["map", "compact map"], rightAnswerIndex: 1),
        SwiftQuestion(question: "What is the full name of CG?", answers: ["Complex Graphic", "Core Graphic"], rightAnswerIndex: 1),
        SwiftQuestion(question: "Which calls first?", answers: ["loadView()", "didLayoutSubview()"], rightAnswerIndex: 0),
        SwiftQuestion(question: "What's the difference between nil and .none?", answers: ["none", ".none used for reactive"], rightAnswerIndex: 0),
        SwiftQuestion(question: "Which one is proper at Swift?", answers: ["if let x...", "guard if.."], rightAnswerIndex: 0),
        SwiftQuestion(question: "Which one is proper at Swift?", answers: ["if case let x...", "a= 5"], rightAnswerIndex: 0),
        SwiftQuestion(question: "Which one is proper at Swift?", answers: ["a += 1", "a++"], rightAnswerIndex: 0),
        SwiftQuestion(question: "Which one is proper at Swift?", answers: ["bool x", "var x"], rightAnswerIndex: 1),
        SwiftQuestion(question: "What is the correct output? true && true", answers: ["true", "false"], rightAnswerIndex: 0),
        SwiftQuestion(question: "What is the correct output? false && true", answers: ["true", "false"], rightAnswerIndex: 1),
        SwiftQuestion(question: "What is the correct output? false || true && true", answers: ["true", "false"], rightAnswerIndex: 0),
        SwiftQuestion(question: "What is the correct output? true && false && false || true", answers: ["true", "false"], rightAnswerIndex: 0),
        SwiftQuestion(question: "What is the correct output? false || false && false", answers: ["true", "false"], rightAnswerIndex: 1),
    ]
}

final class QuestionManager {
    
    enum ContentType: String, CaseIterable {
        case math
        case swift
        case mixed
    }
    
    private func questions(of type: ContentType) -> [Question] {
        switch type {
        case .math:
            return MathQuestionStorage.questions.shuffled()
        case .swift:
            return SwiftQuestionStorage.questions.shuffled()
        case .mixed:
            return (MathQuestionStorage.questions + SwiftQuestionStorage.questions).shuffled()
        }
    }
    
    func questions(for type: ContentType, amount: Int = 10) -> [Question] {
        Array(questions(of: type).prefix(amount))
    }
}
