//
//  Question.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 8.08.2022.
//

import Foundation

protocol Question {
    
    static var topic: String { get }
    var question: String { get }
    var answers: [String] { get }
    var rightAnswerIndex: Int { get }
}
