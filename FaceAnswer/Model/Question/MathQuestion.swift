//
//  MathQuestion.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 8.08.2022.
//

import Foundation

struct MathQuestion: Question {
    
    static let topic = "Math"
    var question: String
    var answers: [String]
    var rightAnswerIndex: Int
}
