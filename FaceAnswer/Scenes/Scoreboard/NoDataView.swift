//
//  NoDataView.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 12.08.2022.
//

import UIKit

final class NoDataView: UIView {
    
    private var label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViews()
        loadConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Views

private extension NoDataView {
    
    func loadViews() {
        loadLabel()
    }
    
    func loadLabel() {
        label = UILabel()
        label.text = "There is no recorded data"
        label.font = .systemFont(ofSize: 24)
        label.textColor = .black
        label.minimumScaleFactor = 0.6
        addSubview(label)
    }
}

// MARK: - Constraints

private extension NoDataView {
    
    func loadConstraints() {
        loadLabelConstraints()
    }
    
    func loadLabelConstraints() {
        label.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.lessThanOrEqualToSuperview().inset(16)
        }
    }
}
