//
//  ScoreboardViewController.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 9.08.2022.
//

import UIKit

final class ScoreboardViewController: UIViewController {
    
    private var scoreboardTableView: UITableView!
    private var noDataView: NoDataView!
    
    private var viewModel = ScoreboardViewModel()
    
    override func loadView() {
        super.loadView()
        loadViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadConstraints()
        viewModel.fetchScores { _ in
            scoreboardTableView.reloadData()
        }
    }
}

// MARK: - View

private extension ScoreboardViewController {
    
    func loadViews() {
        loadNoDataView()
        loadScoreboardTableView()
    }
    
    func loadNoDataView() {
        noDataView = NoDataView()
    }
    
    func loadScoreboardTableView() {
        scoreboardTableView = UITableView()
        scoreboardTableView.allowsSelection = false
        scoreboardTableView.dataSource = self
        scoreboardTableView.backgroundView = noDataView
        scoreboardTableView.register(ScoreboardTableViewCell.self, forCellReuseIdentifier: ScoreboardTableViewCell.description())
        view.addSubview(scoreboardTableView)
    }
}

// MARK: - Constraints

private extension ScoreboardViewController {
    func loadConstraints() {
        loadScoreboardTableViewConstraints()
    }
    
    func loadScoreboardTableViewConstraints() {
        scoreboardTableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

// MARK: - UITableViewDataSource

extension ScoreboardViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if viewModel.scores.isEmpty {
            noDataView.isHidden = false
            return 0
        } else {
            noDataView.isHidden = true 
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.scores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ScoreboardTableViewCell.description(),
                                                       for: indexPath) as? ScoreboardTableViewCell else { return  UITableViewCell() }
        
        if viewModel.scores[indexPath.row] == viewModel.lastScore {
            cell.prepareLastScore(with: viewModel.scores[indexPath.row])
        } else {
            cell.prepare(with: viewModel.scores[indexPath.row])
        }
        
        return cell
    }
}
