//
//  ScorboardTableViewCell.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 9.08.2022.
//

import UIKit

final class ScoreboardTableViewCell: UITableViewCell {
    
    private var scoreLabel: UILabel!
    private var usernameLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        loadViews()
        loadConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - View

private extension ScoreboardTableViewCell {
    
    func loadViews() {
        loadUsernameLabel()
        loadScoreLabel()
    }
    
    func loadUsernameLabel() {
        usernameLabel = UILabel()
        usernameLabel.font = .systemFont(ofSize: 12)
        addSubview(usernameLabel)
    }
    
    func loadScoreLabel() {
        scoreLabel = UILabel()
        scoreLabel.font = .systemFont(ofSize: 18, weight: .semibold)
        addSubview(scoreLabel)
    }
}

// MARK: - Constraints

private extension ScoreboardTableViewCell {
    
    func loadConstraints() {
        loadUsernameLabelConstraints()
        loadScoreLabelConstraints()
    }
    
    func loadUsernameLabelConstraints() {
        usernameLabel.snp.makeConstraints { make in
            make.left.right.top.equalToSuperview().inset(12)
        }
    }
    
    func loadScoreLabelConstraints() {
        scoreLabel.snp.makeConstraints { make in
            make.top.equalTo(usernameLabel.snp.bottom).offset(8)
            make.left.right.bottom.equalToSuperview().inset(12)
        }
    }
}

// MARK: - Helpers

extension ScoreboardTableViewCell {
    
    func prepare(with score: Score) {
        backgroundColor = .white
        fillLabels(with: score)
    }
    
    func prepareLastScore(with score: Score) {
        backgroundColor = .yellow.withAlphaComponent(0.3)
        fillLabels(with: score)
    }
    
    private func fillLabels(with score: Score) {
        usernameLabel.text = "\(score.username)"
        scoreLabel.text = "Score: \(score.score)"
    }
}
