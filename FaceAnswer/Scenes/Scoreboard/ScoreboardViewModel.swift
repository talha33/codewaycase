//
//  ScoreboardViewModel.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 11.08.2022.
//

import Foundation

final class ScoreboardViewModel {
    
    var lastScore: Score?
    var scores: [Score] = []
    
    func fetchScores(completion: (_ scores: [Score]) -> Void) {
        scores = CoreDataManager.getScoreData().sorted(by: { $0.score > $1.score } )
        lastScore = scores.filter({$0.username == User.shared.username}).min(by: {$0.date > $1 .date})
        completion(scores)
    }
}
