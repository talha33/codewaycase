//
//  SelectContentViewController.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 9.08.2022.
//

import UIKit
import AVKit

final class SelectContentViewController: UIViewController {
    
    private var usernameLabel: UILabel!
    private var usernameTextField: UITextField!
    private var stackView: UIStackView!
    
    private var viewModel: SelectContentViewModel = SelectContentViewModel()
    
    override func loadView() {
        super.loadView()
        loadViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadConstraints()
    }
}

// MARK: - View

private extension SelectContentViewController {
    
    func loadViews() {
        configureSelf()
        loadUsernameLabel()
        loadUsernameTextField()
        loadStackView()
    }
    
    func configureSelf() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTappedBackground))
        gesture.cancelsTouchesInView = false
        view.addGestureRecognizer(gesture)
        view.backgroundColor = .white
        navigationController?.navigationBar.tintColor = .orange
    }
    
    func loadUsernameLabel() {
        usernameLabel = UILabel()
        usernameLabel.text = "Username"
        view.addSubview(usernameLabel)
    }
    
    func loadUsernameTextField() {
        usernameTextField = UITextField()
        usernameTextField.placeholder = "enter username"
        usernameTextField.backgroundColor = .lightGray.withAlphaComponent(0.3)
        usernameTextField.delegate = self
        usernameTextField.text = viewModel.username
        usernameTextField.textAlignment = .center
        usernameTextField.autocorrectionType = .no
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.addSubview(usernameTextField)
    }
    
    func loadStackView() {
        let selectableViews: [UIView] = QuestionManager.ContentType.allCases.map {
            let view = SelectionView()
            view.text = $0.rawValue
            view.delegate = self
            return view
        }
        stackView = UIStackView(arrangedSubviews: selectableViews)
        stackView.axis = .vertical
        stackView.spacing = 20
        view.addSubview(stackView)
    }
}

// MARK: - Constraints

private extension SelectContentViewController {
    
    func loadConstraints() {
        loadUsernameLabelConstraints()
        loadUsernameTextFieldConstraints()
        loadStackViewConstraints()
    }
    
    func loadUsernameLabelConstraints() {
        usernameLabel.snp.makeConstraints { make in
            make.bottom.equalTo(usernameTextField.snp.top).offset(-8)
            make.centerX.equalToSuperview()
        }
    }
    
    func loadUsernameTextFieldConstraints() {
        usernameTextField.snp.makeConstraints { make in
            make.bottom.equalTo(stackView.snp.top).offset(-20)
            make.left.right.lessThanOrEqualToSuperview().priority(.low)
            make.centerX.equalToSuperview()
        }
    }
    
    func loadStackViewConstraints() {
        stackView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}

// MARK: - SelectionViewDelegate

extension SelectContentViewController: SelectionViewDelegate {
    
    func selectionViewDidTap(_ selectionView: SelectionView) {
        guard viewModel.hasValidUsername else {
            presentErrorAlert(with: "You have to set a valid username to play the game.\n2 - 15 length\nOnly alphanumerics")
            return
        }
        guard let index = stackView.arrangedSubviews.firstIndex(of: selectionView) else { return }
        let selectedContent = QuestionManager.ContentType.allCases[index]
        navigateToGameIfAuthorized(content: selectedContent)
        
    }
}

// MARK: - UITextFieldDelegate

extension SelectContentViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        !viewModel.isUsernameContainsInvalidChar(string)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        viewModel.username = textField.text
    }
}

// MARK: - Routing

private extension SelectContentViewController {
    
    func checkCameraAuthorization(completionHandler: @escaping (Bool) -> Void) {
        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: completionHandler)
    }
    
    func openApplicationSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl)
        }
    }
    
    func presentCameraPermissionAlert() {
        let controller = UIAlertController(title: "Warning", message: "You have to give camera permission to take quiz", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        let goSettingsAction = UIAlertAction(title: "Open Settings", style: .default, handler: {_ in
            self.openApplicationSettings()
        })
        controller.addAction(cancelAction)
        controller.addAction(goSettingsAction)
        present(controller, animated: true)
    }
    
    func navigateToGameIfAuthorized(content: QuestionManager.ContentType) {
        checkCameraAuthorization { authorized in
            DispatchQueue.main.async {
                if authorized {
                    self.navigateToGame(content: content)
                } else {
                    self.presentCameraPermissionAlert()
                }
            }
        }
    }
    
    func navigateToGame(content: QuestionManager.ContentType) {
        let gameViewController = GameViewController(content: content)
        gameViewController.modalPresentationStyle = .fullScreen
        gameViewController.modalTransitionStyle = .crossDissolve
        present(gameViewController, animated: true)
        navigationController?.popViewController(animated: false)
    }
}

// MARK: - Target Actions

@objc private extension SelectContentViewController {
    
    func didTappedBackground() {
        usernameTextField.resignFirstResponder()
    }
}
