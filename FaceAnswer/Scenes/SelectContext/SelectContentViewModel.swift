//
//  SelectContentViewModel.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 10.08.2022.
//

import Foundation

final class SelectContentViewModel {
    
    var username: String? {
        get { User.shared.username }
        set { User.shared.username = newValue }
    }
    
    var hasValidUsername: Bool { isValidUsername(username ?? "") }
    
    func isValidUsername(_ text: String) -> Bool {
        guard (2...15).contains(text.count) else { return false }
        return !isUsernameContainsInvalidChar(text)
    }
    
    func isUsernameContainsInvalidChar(_ username: String) -> Bool {
        let invalidChars = CharacterSet.alphanumerics.inverted
        return username.rangeOfCharacter(from: invalidChars) != nil
    }
}
