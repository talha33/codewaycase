//
//  HomeViewController.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 8.08.2022.
//

import UIKit

final class HomeViewController: UIViewController {
    
    private var playButton: SelectionView!
    private var scoreboardButton: SelectionView!
    private var stackView: UIStackView!
    
    override func loadView() {
        super.loadView()
        loadViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.stackView.transform = CGAffineTransform.identity
        }
    }
}

// MARK: - View

private extension HomeViewController {
    
    func loadViews() {
        configureSelf()
        loadStackView()
        loadPlayButton()
        loadScoreboardButton()
        
    }
    
    func configureSelf() {
        view.backgroundColor = .white
    }
    
    func loadPlayButton() {
        playButton = SelectionView()
        playButton.text = "Play"
        playButton.textColor = .white
        playButton.delegate = self
        stackView.addArrangedSubview(playButton)
    }
    
    func loadScoreboardButton() {
        scoreboardButton = SelectionView()
        scoreboardButton.text = "Scoreboard"
        scoreboardButton.textColor = .white
        scoreboardButton.delegate = self
        stackView.addArrangedSubview(scoreboardButton)
    }
    
    func loadStackView() {
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 20
        stackView.transform = CGAffineTransform.identity.scaledBy(x: 0, y: 0)
        view.addSubview(stackView)
    }
}

// MARK: - Constraints

private extension HomeViewController {
    
    func loadConstraints() {
        loadStackViewConstraints()
    }
    
    func loadStackViewConstraints() {
        stackView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}

// MARK: - SelectionViewDelegate

extension HomeViewController: SelectionViewDelegate {
    
    func selectionViewDidTap(_ selectionView: SelectionView) {
        
        switch selectionView {
        case playButton:
            navigateToContentSelection()
        case scoreboardButton:
            presentScoreboard()
        default:
            return
        }
    }
}

// MARK: - Routing

private extension HomeViewController {
    
    func navigateToContentSelection() {
        let selectContentViewController = SelectContentViewController()
        navigationController?.pushViewController(selectContentViewController, animated: true)
    }
    
    func presentScoreboard() {
        let scoreboardViewController = ScoreboardViewController()
        present(scoreboardViewController, animated: true)
    }
}
