//
//  UserDetectionViewController.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 8.08.2022.
//

import UIKit
import Vision
import AVKit

protocol UserDetectionViewControllerDelegate: AnyObject {
    func userDetectionViewController(_ userDetectionViewController: UserDetectionViewController, didDetect direction: UserDetectionViewController.Direction)
}

final class UserDetectionViewController: UIViewController {
    
    private let captureSession = AVCaptureSession()
    private lazy var videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
    private let videoDataOutput = AVCaptureVideoDataOutput()
    
    var isRunning: Bool = false
    
    weak var delegate: UserDetectionViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCaptureInput()
        loadVideoPreviewLayer()
        captureSession.startRunning()
        getCameraFrames()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.videoPreviewLayer.frame = self.view.frame
    }

}

// MARK: - Loaders

private extension UserDetectionViewController {
    
    func loadCaptureInput() {
        let deviceTypes: [AVCaptureDevice.DeviceType] = [.builtInWideAngleCamera, .builtInDualCamera, .builtInTrueDepthCamera]
        guard let device = AVCaptureDevice.DiscoverySession(deviceTypes: deviceTypes,
                                                            mediaType: .video,
                                                            position: .front).devices.first,
              let captureInput = try? AVCaptureDeviceInput(device: device) else { fatalError() }
        captureSession.addInput(captureInput)
    }
    
    func loadVideoPreviewLayer() {
        videoPreviewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(videoPreviewLayer)
    }
    
    func getCameraFrames() {
        let queue = DispatchQueue(label: "face_detection_queue")
        videoDataOutput.setSampleBufferDelegate(self, queue: queue)
        captureSession.addOutput(self.videoDataOutput)
    }
}

// MARK: - AVCaptureVideoDataOutputSampleBufferDelegate

extension UserDetectionViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard isRunning, let frame = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        self.detectFace(in: frame)
    }
}

// MARK: - Helpers

extension UserDetectionViewController {
    
    enum Direction: Int {
        case up
        case down
    }
    
    private func handleFaceDetectionResults(_ observedFaces: [VNFaceObservation]) {
        guard let face = observedFaces.first,
              let pitch = face.pitch?.doubleValue else { return }
        
        if pitch > 0.25 {
            delegate?.userDetectionViewController(self, didDetect: .down)
        } else if pitch < -0.25 {
            delegate?.userDetectionViewController(self, didDetect: .up)
        }
    }
    
    private func detectFace(in image: CVPixelBuffer) {
        let faceDetectionRequest = VNDetectFaceRectanglesRequest(completionHandler: { (request, error) in
            if let results = request.results as? [VNFaceObservation] {
                DispatchQueue.main.async {
                    self.handleFaceDetectionResults(results)
                }
            }
        })
        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: image, orientation: .leftMirrored, options: [:])
        try? imageRequestHandler.perform([faceDetectionRequest])
    }
}
