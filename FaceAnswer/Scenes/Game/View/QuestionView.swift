//
//  TextView.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 8.08.2022.
//

import UIKit

final class QuestionView: UIView {
    
    private var label: UILabel!
    
    var text: String? {
        get { label.text }
        set { label.text = newValue }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViews()
        loadConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - View

private extension QuestionView {
    
    func loadViews() {
        configureSelf()
        loadLabel()
    }
    
    func configureSelf() {
        backgroundColor = .white
    }
    
    func loadLabel() {
        label = UILabel()
        label.font = .systemFont(ofSize: 24)
        label.textColor = .black
        label.numberOfLines = 0
        label.textAlignment = .center
        self.addSubview(label)
    }
}
// MARK: - Constraint

private extension QuestionView {
    
    func loadConstraints() {
        loadLabelConstraints()
    }
    
    func loadLabelConstraints() {
        label.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(16)
        }
    }
}
