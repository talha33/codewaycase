//
//  GameIntroView.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 10.08.2022.
//

import UIKit

protocol GameIntroViewDelegate: AnyObject {
    
    func gameIntroViewStartButtonDidTap(_ gameIntroView: GameIntroView)
}

final class GameIntroView: UIView {
    
    private var infoLabel: UILabel!
    private var startButton: SelectionView!
    
    weak var delegate: GameIntroViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViews()
        loadConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - View

private extension GameIntroView {
    
    func loadViews() {
        configureSelf()
        loadInfoLabel()
        loadStartButton()
    }
    
    func configureSelf() {
        layer.cornerRadius = 8
        backgroundColor = .orange
    }
    
    func loadInfoLabel() {
        infoLabel = UILabel()
        infoLabel.text = "Welcome to FaceAnswer.\n There will be 10 questions about the selected topic.\nYou can select answers by moving your face horizontally.\nYou have to select an answer in 10 seconds.\nThe game will start immediately."
        infoLabel.numberOfLines = 0
        infoLabel.textColor = .white
        infoLabel.font = .systemFont(ofSize: 14, weight: .semibold)
        infoLabel.textAlignment = .center
        addSubview(infoLabel)
    }
    
    func loadStartButton() {
        startButton = SelectionView()
        startButton.delegate = self
        startButton.text = "Start Game"
        addSubview(startButton)
    }
}

// MARK: - Constraints

private extension GameIntroView {
    
    func loadConstraints() {
        loadInfoLabelConstraints()
        loadStartButtonConstraints()
    }
    
    func loadInfoLabelConstraints() {
        infoLabel.snp.makeConstraints { make in
            make.left.right.top.equalToSuperview().inset(12)
        }
    }
    
    func loadStartButtonConstraints() {
        startButton.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview().inset(12)
            make.top.equalTo(infoLabel.snp.bottom).offset(16)
        }
    }
}

// MARK: - SelectionViewDelegate

extension GameIntroView: SelectionViewDelegate {
    
    func selectionViewDidTap(_ selectionView: SelectionView) {
        delegate?.gameIntroViewStartButtonDidTap(self)
    }
}
