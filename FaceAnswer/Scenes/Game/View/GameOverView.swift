//
//  GameOverView.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 11.08.2022.
//

import UIKit

protocol GameOverViewDelegate: AnyObject {
    func gameOverViewDidTappedDone()
    func gameOverViewDidTappedScoreboard()
}

final class GameOverView: UIView {
    
    private var scoreView: SelectionView!
    private var doneButton: SelectionView!
    private var scoreboardButton: SelectionView!
    private var stackView: UIStackView!
    
    weak var delegate: GameOverViewDelegate?
    
    private var score: Int
    
    init(score: Int) {
        self.score = score
        super.init(frame: .zero)
        loadViews()
        loadConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Views

extension GameOverView {
    
    func loadViews() {
        configureSelf()
        loadStackView()
        loadScoreView()
        loadDoneButton()
        loadScoreboardButton()
    }
    
    func configureSelf() {
        backgroundColor = .gray.withAlphaComponent(0.6)
    }
    
    func loadStackView() {
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 20
        addSubview(stackView)
    }
    
    func loadScoreView() {
        scoreView = SelectionView()
        scoreView.text = "Score: \(score)"
        stackView.addArrangedSubview(scoreView)
    }
    
    func loadDoneButton() {
        doneButton = SelectionView()
        doneButton.text = "Done"
        doneButton.delegate = self
        stackView.addArrangedSubview(doneButton)
    }
    
    func loadScoreboardButton() {
        scoreboardButton = SelectionView()
        scoreboardButton.text = "Scoreboard"
        scoreboardButton.delegate = self
        stackView.addArrangedSubview(scoreboardButton)
    }
}


// MARK: - Constraints

private extension GameOverView {
    
    func loadConstraints() {
        loadStackViewContrsints()
    }
    
    func loadStackViewContrsints() {
        stackView.snp.makeConstraints { make in
            make.width.equalToSuperview().multipliedBy(0.8)
            make.center.equalToSuperview()
        }
    }
}

extension GameOverView: SelectionViewDelegate {
    
    func selectionViewDidTap(_ selectionView: SelectionView) {
        if selectionView == doneButton {
            delegate?.gameOverViewDidTappedDone()
        } else if selectionView == scoreboardButton {
            delegate?.gameOverViewDidTappedScoreboard()
        }
    }
}
