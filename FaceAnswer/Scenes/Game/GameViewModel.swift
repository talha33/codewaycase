//
//  GameViewModel.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 10.08.2022.
//

import UIKit

protocol GameViewModelDelegate: AnyObject {
    
    func updateProgres(progress: Float)
    func updateDetectionState(isRunning: Bool)
    func updateQuestionViews(with questionString: String, answers: [String])
    func updateAnswersView(at index: Int, with color: UIColor)
    func gameHasEnded(with score: Int)
    func playSound(with url: URL?)
}

final class GameViewModel {
    
    private var gameManager: GameManager
    
    weak var delegate: GameViewModelDelegate?
    
    init(content: QuestionManager.ContentType) {
        gameManager = GameManager(content: content)
        gameManager.delegate = self
    }
    
    enum Constant {
        
        enum AnswerSounds {
            
            static let correctURL = Bundle.main.url(forResource: "correct", withExtension: "mp3")
            static let wrongURL = Bundle.main.url(forResource: "wrong", withExtension: "mp3")
            static let missedURL = Bundle.main.url(forResource: "missed", withExtension: "mp3")
        }
        
        enum AnswerColor {
            
            static let correct = UIColor.green
            static let wrong = UIColor.red
            static let missed = UIColor.darkGray
        }
    }
    
    func startGame() {
        gameManager.startGame()
        delegate?.updateDetectionState(isRunning: true)
    }
    
    func checkAnswer(at index: Int) {
        let answer = gameManager.checkAnswer(with: index)
        switch answer {
        case .correct(let correctIndex):
            delegate?.updateAnswersView(at: correctIndex, with: Constant.AnswerColor.correct)
            delegate?.playSound(with: Constant.AnswerSounds.correctURL)
        case .wrong(let selectedIndex, let correctIndex):
            delegate?.updateAnswersView(at: selectedIndex, with: Constant.AnswerColor.wrong)
            delegate?.updateAnswersView(at: correctIndex, with: Constant.AnswerColor.correct)
            delegate?.playSound(with: Constant.AnswerSounds.wrongURL)
        default:
            fatalError()
        }
    }
}

// MARK: - GameManagerDelegate

extension GameViewModel: GameManagerDelegate {
    
    func gameManagerTimerProgress(progress: Double) {
        delegate?.updateProgres(progress: Float(progress))
    }
    
    func gameManagerAnswerTimeWillBegin(with question: Question) {
        delegate?.updateQuestionViews(with: question.question, answers: question.answers)
    }
    
    func gameManagerAnswerTimeDidEndWithoutResponse(result: GameManager.AnswerResult) {
        for index in gameManager.currentQuestion.answers.indices {
            if gameManager.currentQuestion.rightAnswerIndex == index {
                delegate?.updateAnswersView(at: index, with: Constant.AnswerColor.correct)
                continue
            }
            delegate?.updateAnswersView(at: index, with: Constant.AnswerColor.missed)
            delegate?.playSound(with: Constant.AnswerSounds.missedURL)
        }
    }
    
    func gameManagerWaitTimeWillBegin() {
        delegate?.updateDetectionState(isRunning: false)
    }
    
    
    func gameManagerWaitTimeDidEnd() {
        delegate?.updateDetectionState(isRunning: true)
    }
    
    func gameManagerGameDidEnd(with score: Int) {
        if let username = User.shared.username {
            let score = Score(score: score, username: username, date: Date())
            CoreDataManager.saveScoreData(with: score)
        }
        delegate?.updateDetectionState(isRunning: false)
        delegate?.gameHasEnded(with: score)
    }
}
