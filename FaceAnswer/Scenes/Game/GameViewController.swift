//
//  ViewController.swift
//  FaceAnswer
//
//  Created by Talha Selimhan Ari on 8.08.2022.
//

import UIKit
import AVFoundation


final class GameViewController: UIViewController {
    
    private var userDetectionViewController: UserDetectionViewController!
    private var questionView: QuestionView!
    private var progressView: UIProgressView!
    private var answersStackView: UIStackView!
    private var player: AVAudioPlayer?
    private var closeButton: UIButton!
    private var gameOverView: GameOverView?
    private weak var gameIntroView: GameIntroView?
    
    private var viewModel: GameViewModel
    
    init(content: QuestionManager.ContentType) {
        viewModel = GameViewModel(content: content)
        super.init(nibName: nil, bundle: nil)
        viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        loadViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadConstraints()
    }
}

// MARK: - Views

private extension GameViewController {
    
    func loadViews() {
        loadUserDetectionViewController()
        loadQuestionView()
        loadProgressView()
        loadCloseButton()
        loadAnswersStackView()
        loadGameIntroView()
    }
    
    func loadUserDetectionViewController() {
        userDetectionViewController = UserDetectionViewController()
        userDetectionViewController.delegate = self
        addChild(userDetectionViewController)
        view.addSubview(userDetectionViewController.view)
        userDetectionViewController.didMove(toParent: self)
    }
    
    func loadQuestionView() {
        questionView = QuestionView()
        questionView.text = "Test"
        view.addSubview(questionView)
    }
    
    func loadProgressView() {
        progressView = UIProgressView()
        progressView.progressTintColor = .blue
        progressView.trackTintColor = .orange
        view.addSubview(progressView)
    }
    
    func loadCloseButton() {
        closeButton = UIButton()
        closeButton.setImage(UIImage(systemName: "xmark")?.withTintColor(.white), for: .normal)
        closeButton.backgroundColor = .gray.withAlphaComponent(0.4)
        closeButton.layer.cornerRadius = .infinity
        closeButton.addTarget(self, action: #selector(closeButtonDidTap), for: .touchUpInside)
        view.addSubview(closeButton)
    }
    
    func loadAnswersStackView() {
        answersStackView = UIStackView()
        answersStackView.axis = .vertical
        answersStackView.spacing = 20
        view.addSubview(answersStackView)
    }
    
    func loadGameOverView(with score: Int) {
        let gameOverView = GameOverView(score: score)
        gameOverView.delegate = self
        self.gameOverView = gameOverView
        view.addSubview(gameOverView)
    }
    
    func loadGameIntroView() {
        let gameIntroView = GameIntroView()
        gameIntroView.delegate = self
        view.addSubview(gameIntroView)
        self.gameIntroView = gameIntroView
    }
}

// MARK: - Constraints

private extension GameViewController {
    
    func loadConstraints() {
        loadUserDetectionViewControllerConstraints()
        loadQuestionViewConstraints()
        loadProgressViewConstraints()
        loadCloseButtonConstraints()
        loadAnswersStackViewConstraints()
        loadStartButtonConstraints()
    }
    
    func loadUserDetectionViewControllerConstraints() {
        userDetectionViewController.view.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaLayoutGuide.snp.edges)
        }
    }
    
    
    func loadQuestionViewConstraints() {
        questionView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
        }
    }
    
    func loadProgressViewConstraints() {
        progressView.snp.makeConstraints { make in
            make.top.equalTo(questionView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(8)
        }
    }
    
    func loadCloseButtonConstraints() {
        closeButton.snp.makeConstraints { make in
            make.top.equalTo(progressView.snp.bottom).offset(4)
            make.left.equalToSuperview().inset(4)
            make.size.equalTo(44)
        }
    }
    
    func loadAnswersStackViewConstraints() {
        answersStackView.snp.makeConstraints { make in
            make.bottom.left.right.equalToSuperview()
        }
    }
    
    func loadGameOverViewContraints() {
        gameOverView?.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func loadStartButtonConstraints() {
        gameIntroView?.snp.makeConstraints { make in
            make.width.equalToSuperview().multipliedBy(0.9)
            make.center.equalToSuperview()
        }
    }
}

// MARK: - UserDetectionViewControllerDelegate

extension GameViewController: UserDetectionViewControllerDelegate {
    
    func userDetectionViewController(_ userDetectionViewController: UserDetectionViewController, didDetect direction: UserDetectionViewController.Direction) {
        viewModel.checkAnswer(at: direction.rawValue)
    }
}

// MARK: - GameViewModelDelegate

extension GameViewController: GameViewModelDelegate {
    
    func playSound(with url: URL?) {
        guard let url = url else { return }
        
        player = try? AVAudioPlayer(contentsOf: url)
        player?.play()
    }
    
    func updateAnswersView(at index: Int, with color: UIColor) {
        answersStackView.subviews[index].backgroundColor = color
    }
    
    func updateProgres(progress: Float) {
        progressView.progress = progress
    }
    
    func updateDetectionState(isRunning: Bool) {
        userDetectionViewController.isRunning = isRunning
    }
    
    func updateQuestionViews(with questionString: String, answers: [String]) {
        questionView.text = questionString
        answersStackView.removeAllSubviews()
        answers.forEach { answer in
            let textView = QuestionView()
            textView.text = answer
            answersStackView.addArrangedSubview(textView)
        }
    }
    
    func gameHasEnded(with score: Int) {
        loadGameOverView(with: score)
        loadGameOverViewContraints()
    }
}

// MARK: - GameIntroViewDelegate

extension GameViewController: GameIntroViewDelegate {
    
    func gameIntroViewStartButtonDidTap(_ gameIntroView: GameIntroView) {
        viewModel.startGame()
        gameIntroView.removeFromSuperview()
    }
}

extension GameViewController: GameOverViewDelegate {
    func gameOverViewDidTappedDone() {
        dismiss(animated: true)
    }
    
    func gameOverViewDidTappedScoreboard() {
        let scoreboardViewController = ScoreboardViewController()
        present(scoreboardViewController, animated: true)
    }
}

// MARK: - Target Actions

@objc private extension GameViewController {

    func closeButtonDidTap() {
        dismiss(animated: true)
    }
}

